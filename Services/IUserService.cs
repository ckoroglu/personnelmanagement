using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using PersonnelManagement.Models;
using PersonnelManagement.Models.ViewModels;
using Task = System.Threading.Tasks.Task;

namespace PersonnelManagement.Services
{
    public interface IUserService
    {
        Task<Employee> Authenticate(string username, string password);
    }

    public class UserService : IUserService
    {
        private static PmDbContext _dbContext;

        public UserService(PmDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        public async Task<Employee> Authenticate(string email, string password)
        {
            var user = await Task.Run(() => _dbContext.Employees.FirstOrDefault(x => x.Email == email && x.Password == password));

            if (user == null)
                return null;
            return user;
        }
        
        public static Employee GetUser(AuthenticationHeaderValue headerValue, PmDbContext context)
        {
            string encodedUsernamePassword = headerValue.ToString().Substring("Basic ".Length).Trim();
            Encoding encoding = Encoding.GetEncoding("iso-8859-1");
            string usernamePassword = encoding.GetString(Convert.FromBase64String(encodedUsernamePassword));
            
            int seperatorIndex = usernamePassword.IndexOf(':');

            var email = usernamePassword.Substring(0, seperatorIndex);
            var password = usernamePassword.Substring(seperatorIndex + 1);

            return context.Employees.FirstOrDefault(x => x.Email == email && x.Password == password);
        }
        
    }
}