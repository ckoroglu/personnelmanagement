using System.Linq;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace PersonnelManagement.Models
{
    public class PmDbContext : DbContext
    {
        public DbSet<LeaveType> LeaveTypes { get; set; }
        public DbSet<Leave> Leaves { get; set; }
        public DbSet<Manager> Managers { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Project> Projects { get; set; }

        public PmDbContext(DbContextOptions<PmDbContext> options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySql("server=personnelmanagement.c78t7cbcxzbl.eu-central-1.rds.amazonaws.com;port=3306;database=personnelmanagement;uid=root;password=1q2w3easd");
        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
            var cascadeFKs = modelBuilder.Model.GetEntityTypes()
                .SelectMany(t => t.GetForeignKeys())
                .Where(fk => !fk.IsOwnership && fk.DeleteBehavior == DeleteBehavior.Cascade);

            foreach (var fk in cascadeFKs)
            {
                fk.DeleteBehavior = DeleteBehavior.Restrict;
            }

            modelBuilder.Entity<Task>()
                .HasOne(m => m.AssignedEmployee)
                .WithMany(t => t.AssignedTasks)
                .HasForeignKey(m => m.AssignedEmployeeId);

            modelBuilder.Entity<Task>()
                .HasOne(m => m.AssignerEmployee)
                .WithMany(t => t.CreatedTasks)
                .HasForeignKey(m => m.AssignerEmployeeId);
            
            modelBuilder.Entity<Employee>()
                .HasIndex(u => u.Email)
                .IsUnique();

        }
        
    }
}