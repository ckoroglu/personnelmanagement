using System;

namespace PersonnelManagement.Models.ViewModels
{
    public class TaskViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Type { get; set; }
        public int Priority { get; set; }
        public int Status { get; set; }
        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset EndDate { get; set; }
        public string ProjectName { get; set; }
        
        public Guid AssignedEmployeeId { get; set; }
        public string AssignerEmployeeName { get; set; }
        public string AssignedEmployeeName { get; set; }
    }
}