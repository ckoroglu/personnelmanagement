using System;

namespace PersonnelManagement.Models.ViewModels
{
    public class LeaveTypeViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public bool Type { get; set; }
    }
}