namespace PersonnelManagement.Models.ViewModels
{
    public class ManagerDashboardViewModel
    {
        public int TotalProject { get; set; }
        public int TotalEmployee { get; set; }
        public int TotalTask { get; set; }
        public int TotalLeaveRequest { get; set; }
    }
}