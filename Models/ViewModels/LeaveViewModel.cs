using System;

namespace PersonnelManagement.Models.ViewModels
{
    public class LeaveViewModel
    {
        public Guid Id { get; set; }
        public string Reason { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset EndDate { get; set; }
        public string Remark { get; set; }
        public int Status { get; set; }
        public string ManagerName { get; set; }
        public string LeaveType { get; set; }
        public string EmployeeName { get; set; }
    }
}