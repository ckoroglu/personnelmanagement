using System.Collections.Generic;

namespace PersonnelManagement.Models.ViewModels
{
    public class Result<T>
    {
        public bool Success { get; set; }
        public string Error { get; set; } 
        public T Data { get; set; }
        
        public Result(bool success, string error, T data)
        {
            Success = success;
            Error = error;
            Data = data;
        }

        public Result()
        {
            
        }
    }
}