using System;

namespace PersonnelManagement.Models.ViewModels
{
    public class DepartmentViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}