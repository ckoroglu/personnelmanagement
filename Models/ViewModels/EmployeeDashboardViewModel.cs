namespace PersonnelManagement.Models.ViewModels
{
    public class EmployeeDashboardViewModel
    {
        public int TotalProject { get; set; }
        public int TotalTask { get; set; }
        public int TotalLeaveRequest { get; set; }
    }
}