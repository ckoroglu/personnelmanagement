using System;

namespace PersonnelManagement.Models.ViewModels
{
    public class UserViewModel
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTimeOffset StartDate { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public int Gender { get; set; }
        public int Status { get; set; }
        public int Salary { get; set; }
        public string Role { get; set; }
        public string Department { get; set; }
        public int WorkingDayPerMonth { get; set; }
    }
}