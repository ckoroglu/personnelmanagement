using System;
using System.Collections.Generic;

namespace PersonnelManagement.Models
{
    public class Leave
    {
        public Guid Id { get; set; }
        public string Reason { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset EndDate { get; set; }
        public string Remark { get; set; }
        public int Status { get; set; }
        
        public Guid? ManagerId { get; set; }
        public Guid LeaveTypeId { get; set; }
        public Guid EmployeeId { get; set; }
        
        public Manager Manager { get; set; }
        public LeaveType LeaveType { get; set; }
        public Employee Employee { get; set; }
    }
}