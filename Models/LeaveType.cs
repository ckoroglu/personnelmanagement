using System;
using System.Collections.Generic;

namespace PersonnelManagement.Models
{
    public class LeaveType
    {

        public LeaveType()
        {
            Leaves = new HashSet<Leave>();
        }
        
        public Guid Id { get; set; }
        public string Name { get; set; }
        public bool Type { get; set; }
        
        public ICollection<Leave> Leaves { get; set; }
    }
}