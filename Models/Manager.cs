using System;
using System.Collections.Generic;

namespace PersonnelManagement.Models
{
    public class Manager
    {
        
        public Guid Id { get; set; }
        public Guid EmployeeId { get; set; }
        public Employee Employee { get; set; }
    }
}