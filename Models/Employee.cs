using System;
using System.Collections.Generic;

namespace PersonnelManagement.Models
{
    public class Employee
    {
        
        public Employee()
        {
            Leaves = new HashSet<Leave>();
            CreatedTasks = new HashSet<Task>();
            AssignedTasks = new HashSet<Task>();
            Projects = new HashSet<Project>();
            Managers = new HashSet<Manager>();
        }
        
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public DateTimeOffset StartDate { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public int Gender { get; set; }
        public int Salary { get; set; }
        public int WorkingDayPerMonth { get; set; }
        
        public Guid DepartmentId { get; set; }
        public Department Department { get; set; }
        
        public ICollection<Leave> Leaves { get; set; }
        public ICollection<Project> Projects { get; set; }
        public ICollection<Task> CreatedTasks { get; set; }
        public ICollection<Task> AssignedTasks { get; set; }
        public ICollection<Manager> Managers { get; set; }
    }
}