using System;
using System.Collections.Generic;

namespace PersonnelManagement.Models
{
    public class Project
    {

        public Project()
        {
            Tasks = new HashSet<Task>();
        }
        
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }
        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset EndDate { get; set; }
        
        public Guid EmployeeId { get; set; }
        public Employee Employee { get; set; }
        
        public ICollection<Task> Tasks { get; set; }
        
    }
}