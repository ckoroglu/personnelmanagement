using System;

namespace PersonnelManagement.Models.RequestModels
{
    public class TaskRequest
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int Type { get; set; }
        public int Priority { get; set; }
        public int Status { get; set; }
        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset EndDate { get; set; }
        
        public Guid ProjectId { get; set; }
        public Guid AssignedEmployeeId { get; set; }
    }
}