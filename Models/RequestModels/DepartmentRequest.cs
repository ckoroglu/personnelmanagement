namespace PersonnelManagement.Models.RequestModels
{
    public class DepartmentRequest
    {
        public string Name { get; set; }
    }
}