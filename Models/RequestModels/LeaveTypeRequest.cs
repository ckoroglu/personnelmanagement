namespace PersonnelManagement.Models.RequestModels
{
    public class LeaveTypeRequest
    {
        public string Name { get; set; }
        public bool Type { get; set; }
    }
}