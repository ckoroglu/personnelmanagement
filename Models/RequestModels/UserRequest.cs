using System;

namespace PersonnelManagement.Models.RequestModels
{
    public class UserRequest
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public DateTimeOffset StartDate { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public int Gender { get; set; }
        public int Salary { get; set; }
        public int WorkingDayPerMonth { get; set; }
        public Guid DepartmentId { get; set; }
        public string Role { get; set; }
    }
}