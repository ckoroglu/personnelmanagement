using System;

namespace PersonnelManagement.Models.RequestModels
{
    public class ProjectRequest
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset EndDate { get; set; }
        public int Status { get; set; }
    }
}