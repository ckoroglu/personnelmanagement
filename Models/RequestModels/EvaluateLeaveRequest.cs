namespace PersonnelManagement.Models.RequestModels
{
    public class EvaluateLeaveRequest
    {
        public string Remark { get; set; }
        public int Status { get; set; }
    }
}