using System;

namespace PersonnelManagement.Models.RequestModels
{
    public class LeaveRequest
    {
        public string Reason { get; set; }
        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset EndDate { get; set; }
        public Guid LeaveTypeId { get; set; }
    }
}