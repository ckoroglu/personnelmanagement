using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace PersonnelManagement.Models
{
    public class Task
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Type { get; set; }
        public int Priority { get; set; }
        public int Status { get; set; }
        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset EndDate { get; set; }
        
        public Guid ProjectId { get; set; }
        public Project Project { get; set; }
        
        public Guid AssignerEmployeeId { get; set; }
        public Guid AssignedEmployeeId { get; set; }
        
        public Employee AssignedEmployee { get; set; }
        public Employee AssignerEmployee { get; set; }
        
//        public Guid AssignedEmployeeId { get; set; }
//        [ForeignKey("Employee")]
//        public Employee AssignedEmployee { get; set; }
    }
}