using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using PersonnelManagement.Models;
using PersonnelManagement.Models.RequestModels;
using PersonnelManagement.Models.ViewModels;
using PersonnelManagement.Services;

namespace PersonnelManagement.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class LeaveTypeController : Controller
    {
        private readonly PmDbContext _dbContext;

        public LeaveTypeController(PmDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        [HttpPost]
        [ProducesResponseType(typeof(Result<List<LeaveTypeViewModel>>), 200)]
        public async Task<IActionResult> Create([FromBody] LeaveTypeRequest leaveTypeRequest)
        {
            
            var user = UserService.GetUser(AuthenticationHeaderValue.Parse(Request.Headers[HeaderNames.Authorization]), _dbContext);

            if (!_dbContext.Managers.Any(manager => manager.Employee == user))
            {
                
                HttpContext.Response.StatusCode = 401;
                return null;
            }
            
            _dbContext.LeaveTypes.Add(new LeaveType
            {
                Id = Guid.NewGuid(),
                Name = leaveTypeRequest.Name,
                Type = leaveTypeRequest.Type
            });

            await _dbContext.SaveChangesAsync();
            var leaveTypes = _dbContext.LeaveTypes.Select(leaveType => new LeaveTypeViewModel
            {
                Id = leaveType.Id,
                Name = leaveType.Name,
                Type = leaveType.Type
            }).ToList();
            
            return Ok(new Result<List<LeaveTypeViewModel>>
            (
                success: true,
                error: "",
                data: leaveTypes
            ));
        }
        
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(Result<List<LeaveTypeViewModel>>), 200)]
        public async Task<IActionResult> Update(Guid id, [FromBody] LeaveTypeRequest leaveTypeRequest)
        {
            
            var user = UserService.GetUser(AuthenticationHeaderValue.Parse(Request.Headers[HeaderNames.Authorization]), _dbContext);

            if (!_dbContext.Managers.Any(manager => manager.Employee == user))
            {
                
                HttpContext.Response.StatusCode = 401;
                return null;
            }

            var leaveTypeEntity = _dbContext.LeaveTypes.First(leaveType => leaveType.Id == id);

            leaveTypeEntity.Name = leaveTypeRequest.Name;
            leaveTypeEntity.Type = leaveTypeRequest.Type;

            await _dbContext.SaveChangesAsync();
            
            var leaveTypes = _dbContext.LeaveTypes.Select(leaveType => new LeaveTypeViewModel
            {
                Id = leaveType.Id,
                Name = leaveType.Name,
                Type = leaveType.Type
            }).ToList();
            
            return Ok(new Result<List<LeaveTypeViewModel>>
            (
                success: true,
                error: "",
                data: leaveTypes
            ));
        }
        
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(Result<List<LeaveTypeViewModel>>), 200)]
        public async Task<IActionResult> Delete(Guid id)
        {
            
            var user = UserService.GetUser(AuthenticationHeaderValue.Parse(Request.Headers[HeaderNames.Authorization]), _dbContext);

            if (!_dbContext.Managers.Any(manager => manager.Employee == user))
            {
                
                HttpContext.Response.StatusCode = 401;
                return null;
            }

            _dbContext.LeaveTypes.Remove(_dbContext.LeaveTypes.First(leaveType => leaveType.Id == id));

            await _dbContext.SaveChangesAsync();
            
            var leaveTypes = _dbContext.LeaveTypes.Select(leaveType => new LeaveTypeViewModel
            {
                Id = leaveType.Id,
                Name = leaveType.Name,
                Type = leaveType.Type
            }).ToList();
            
            return Ok(new Result<List<LeaveTypeViewModel>>
            (
                success: true,
                error: "",
                data: leaveTypes
            ));
        }
        
        [HttpGet]
        [ProducesResponseType(typeof(Result<List<LeaveTypeViewModel>>), 200)]
        public async Task<IActionResult> Get()
        {
            
//            var user = UserService.GetUser(AuthenticationHeaderValue.Parse(Request.Headers[HeaderNames.Authorization]), _dbContext);
//
//            if (!_dbContext.Managers.Any(manager => manager.Employee == user))
//            {
//                
//                HttpContext.Response.StatusCode = 401;
//                return null;
//            }
            
            var leaveTypes = _dbContext.LeaveTypes.Select(leaveType => new LeaveTypeViewModel
            {
                Id = leaveType.Id,
                Name = leaveType.Name,
                Type = leaveType.Type
            }).ToList();
            
            return Ok(new Result<List<LeaveTypeViewModel>>
            (
                success: true,
                error: "",
                data: leaveTypes
            ));
        }

    }
}