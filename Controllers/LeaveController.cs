using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using PersonnelManagement.Models;
using PersonnelManagement.Models.RequestModels;
using PersonnelManagement.Models.ViewModels;
using PersonnelManagement.Services;

namespace PersonnelManagement.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class LeaveController : ControllerBase
    {
        private readonly PmDbContext _dbContext;

        public LeaveController(IUserService userService, PmDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        [HttpGet("AllLeaveRequests")]
        [ProducesResponseType(typeof(Result<List<LeaveViewModel>>), 200)]
        public IActionResult AllLeaveRequests()
        {
            var user = UserService.GetUser(AuthenticationHeaderValue.Parse(Request.Headers[HeaderNames.Authorization]), _dbContext);

            if (!_dbContext.Managers.Any(manager => manager.Employee == user))
            {
                
                HttpContext.Response.StatusCode = 401;
                return null;
            }
            
            var leaveRequests = _dbContext.Leaves.Select(leave => new LeaveViewModel
            {
                Id = leave.Id,
                Reason = leave.Reason,
                CreatedAt = leave.CreatedAt,
                StartDate = leave.StartDate,
                EndDate = leave.EndDate,
                Remark = leave.Remark,
                Status = leave.Status,
                EmployeeName = leave.Employee.FirstName + " " + leave.Employee.LastName,
                ManagerName = leave.Manager.Employee.FirstName + " " + leave.Manager.Employee.LastName,
                LeaveType = leave.LeaveType.Name
            }).ToList();
            
            return Ok(new Result<List<LeaveViewModel>>
            (
                success: true,
                error: "",
                data: leaveRequests
            ));
        }
        
        [HttpGet("UserLeaves/{id}")]
        [ProducesResponseType(typeof(Result<List<LeaveViewModel>>), 200)]
        public IActionResult UserLeaves(Guid id)
        {
            var user = UserService.GetUser(AuthenticationHeaderValue.Parse(Request.Headers[HeaderNames.Authorization]), _dbContext);

            if (!_dbContext.Managers.Any(manager => manager.Employee == user))
            {
                HttpContext.Response.StatusCode = 401;
                return null;
            }

            var userLeaveRequests = _dbContext.Leaves.Where(leave => leave.Employee.Id == id).Select(leave => new LeaveViewModel
            {
                Id = leave.Id,
                Reason = leave.Reason,
                CreatedAt = leave.CreatedAt,
                StartDate = leave.StartDate,
                EndDate = leave.EndDate,
                Remark = leave.Remark,
                Status = leave.Status,
                EmployeeName = leave.Employee.FirstName + " " + leave.Employee.LastName,
                ManagerName = leave.Manager.Employee.FirstName + " " + leave.Manager.Employee.LastName,
                LeaveType = leave.LeaveType.Name
            }).ToList();
            
            return Ok(new Result<List<LeaveViewModel>>
            (
                success: true,
                error: "",
                data: userLeaveRequests
            ));
        }
        
        [HttpGet]
        [ProducesResponseType(typeof(Result<List<LeaveViewModel>>), 200)]
        public IActionResult LeaveRequests()
        {
            var user = UserService.GetUser(AuthenticationHeaderValue.Parse(Request.Headers[HeaderNames.Authorization]), _dbContext);
            
            var userLeaveRequests = _dbContext.Leaves.Where(leave => leave.Employee == user).Select(leave => new LeaveViewModel
            {
                Id = leave.Id,
                Reason = leave.Reason,
                CreatedAt = leave.CreatedAt,
                StartDate = leave.StartDate,
                EndDate = leave.EndDate,
                Remark = leave.Remark,
                Status = leave.Status,
                EmployeeName = leave.Employee.FirstName + " " + leave.Employee.LastName,
                ManagerName = leave.Manager.Employee.FirstName + " " + leave.Manager.Employee.LastName,
                LeaveType = leave.LeaveType.Name
            }).ToList();
            
            return Ok(new Result<List<LeaveViewModel>>
            (
                success: true,
                error: "",
                data: userLeaveRequests
            ));
        }
        
        [HttpPost]
        [ProducesResponseType(typeof(Result<List<LeaveViewModel>>), 200)]
        public async Task<IActionResult> CreateLeaveRequest([FromBody] LeaveRequest leaveRequest)
        {
            var user = UserService.GetUser(AuthenticationHeaderValue.Parse(Request.Headers[HeaderNames.Authorization]), _dbContext);
            
            _dbContext.Leaves.Add(new Leave
            {
                Id = Guid.NewGuid(),
                Reason = leaveRequest.Reason,
                CreatedAt = DateTimeOffset.UtcNow,
                StartDate = leaveRequest.StartDate,
                EndDate = leaveRequest.EndDate,
                Employee = user,
                LeaveType = _dbContext.LeaveTypes.FirstOrDefault(type => type.Id == leaveRequest.LeaveTypeId),
                Status = 0
            });

            await _dbContext.SaveChangesAsync();
            
            var userLeaveRequests = _dbContext.Leaves.Where(leave => leave.Employee.Id == user.Id).Select(leave => new LeaveViewModel
            {
                Id = leave.Id,
                Reason = leave.Reason,
                CreatedAt = leave.CreatedAt,
                StartDate = leave.StartDate,
                EndDate = leave.EndDate,
                Remark = leave.Remark,
                Status = leave.Status,
                EmployeeName = leave.Employee.FirstName + " " + leave.Employee.LastName,
                ManagerName = leave.Manager.Employee.FirstName + " " + leave.Manager.Employee.LastName,
                LeaveType = leave.LeaveType.Name
            }).ToList();
            
            return Ok(new Result<List<LeaveViewModel>>
            (
                success: true,
                error: "",
                data: userLeaveRequests
            ));
        }
        
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(Result<List<LeaveViewModel>>), 200)]
        public async Task<IActionResult> EvaluateLeaveRequest(Guid id, [FromBody] EvaluateLeaveRequest leaveRequest)
        {
            var user = UserService.GetUser(AuthenticationHeaderValue.Parse(Request.Headers[HeaderNames.Authorization]), _dbContext);

            if (!_dbContext.Managers.Any(manager => manager.Employee == user))
            {
                
                HttpContext.Response.StatusCode = 401;
                return null;
            }

            var leaveData = _dbContext.Leaves.First(leave => leave.Id == id);
            leaveData.Remark = leaveRequest.Remark;
            leaveData.Status = leaveRequest.Status;
            leaveData.Manager = _dbContext.Managers.First(manager => manager.Employee == user);

            await _dbContext.SaveChangesAsync();
            
            var userLeaveRequests = _dbContext.Leaves.Select(leave => new LeaveViewModel
            {
                Id = leave.Id,
                Reason = leave.Reason,
                CreatedAt = leave.CreatedAt,
                StartDate = leave.StartDate,
                EndDate = leave.EndDate,
                Remark = leave.Remark,
                Status = leave.Status,
                EmployeeName = leave.Employee.FirstName + " " + leave.Employee.LastName,
                ManagerName = leave.Manager.Employee.FirstName + " " + leave.Manager.Employee.LastName,
                LeaveType = leave.LeaveType.Name
            }).ToList();
            
            return Ok(new Result<List<LeaveViewModel>>
            (
                success: true,
                error: "",
                data: userLeaveRequests
            ));
        }
        
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(Result<List<LeaveViewModel>>), 200)]
        public async Task<IActionResult> CancelLeaveRequest(Guid id)
        {
            
            var user = UserService.GetUser(AuthenticationHeaderValue.Parse(Request.Headers[HeaderNames.Authorization]), _dbContext);

            var leaveRequest = _dbContext.Leaves.FirstOrDefault(leave => leave.Id == id && leave.Employee == user);

            if (leaveRequest != null)
            {
                _dbContext.Leaves.Remove(leaveRequest);
                await _dbContext.SaveChangesAsync();
            }
            
            var userLeaveRequests = _dbContext.Leaves.Where(leave => leave.Employee.Id == user.Id).Select(leave => new LeaveViewModel
            {
                Id = leave.Id,
                Reason = leave.Reason,
                CreatedAt = leave.CreatedAt,
                StartDate = leave.StartDate,
                EndDate = leave.EndDate,
                Remark = leave.Remark,
                Status = leave.Status,
                EmployeeName = leave.Employee.FirstName + " " + leave.Employee.LastName,
                ManagerName = leave.Manager.Employee.FirstName + " " + leave.Manager.Employee.LastName,
                LeaveType = leave.LeaveType.Name
            }).ToList();
            
            return Ok(new Result<List<LeaveViewModel>>
            (
                success: true,
                error: "",
                data: userLeaveRequests
            ));
        }
        
    }
}