using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Net.Http.Headers;
using PersonnelManagement.Models;
using PersonnelManagement.Models.RequestModels;
using PersonnelManagement.Models.ViewModels;
using PersonnelManagement.Services;
using Task = PersonnelManagement.Models.Task;

namespace PersonnelManagement.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class TaskController : Controller
    {
        private readonly PmDbContext _dbContext;

        public TaskController(IUserService userService, PmDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        /// <summary>
        /// Employee
        /// Deletes a task
        /// </summary>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(Result<List<TaskViewModel>>), 200)]
        public async Task<IActionResult> DeleteTask(Guid id)
        {
            var user = UserService.GetUser(AuthenticationHeaderValue.Parse(Request.Headers[HeaderNames.Authorization]), _dbContext);
            _dbContext.Tasks.Remove(_dbContext.Tasks.First(task => task.Id == id));

            await _dbContext.SaveChangesAsync();
            
            var checkUserManager = _dbContext.Managers.Any(manager => manager.Employee == user);

            if (checkUserManager)
            {
                var tasks = _dbContext.Tasks.Select(task => new TaskViewModel
                {
                    Name = task.Name,
                    Description = task.Description,
                    StartDate = task.StartDate,
                    EndDate = task.EndDate,
                    Status = task.Status,
                    Id = task.Id,
                    Priority = task.Priority,
                    Type = task.Type,
                    AssignerEmployeeName = task.AssignerEmployee.FirstName + " " + task.AssignerEmployee.LastName,
                    AssignedEmployeeName = task.AssignedEmployee.FirstName + " " + task.AssignedEmployee.LastName,
                    AssignedEmployeeId = task.AssignedEmployee.Id,
                    ProjectName = task.Project.Name
                }).ToList();
            
                return Ok(new Result<List<TaskViewModel>>
                (
                    success: true,
                    error: "",
                    data: tasks
                ));
            }
            else
            {
                var tasks = _dbContext.Tasks.Where(task => task.AssignedEmployee == user).Select(task => new TaskViewModel
                {
                    Name = task.Name,
                    Description = task.Description,
                    StartDate = task.StartDate,
                    EndDate = task.EndDate,
                    Status = task.Status,
                    Id = task.Id,
                    Priority = task.Priority,
                    Type = task.Type,
                    AssignerEmployeeName = task.AssignerEmployee.FirstName + " " + task.AssignerEmployee.LastName,
                    AssignedEmployeeName = task.AssignedEmployee.FirstName + " " + task.AssignedEmployee.LastName,
                    AssignedEmployeeId = task.AssignedEmployee.Id,
                    ProjectName = task.Project.Name
                }).ToList();
            
                return Ok(new Result<List<TaskViewModel>>
                (
                    success: true,
                    error: "",
                    data: tasks
                ));
            }
        }
        
        /// <summary>
        /// Employee
        /// Updates a task
        /// </summary>
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(Result<List<TaskViewModel>>), 200)]
        public async Task<IActionResult> UpdateTask(Guid id, [FromBody] TaskRequest taskRequest)
        {
            var user = UserService.GetUser(AuthenticationHeaderValue.Parse(Request.Headers[HeaderNames.Authorization]), _dbContext);
            var taskEntity = _dbContext.Tasks.First(task => task.Id == id);
            taskEntity.Name = taskRequest.Name;
            taskEntity.Description = taskRequest.Description;
            taskEntity.StartDate = taskRequest.StartDate;
            taskEntity.EndDate = taskRequest.EndDate;
            taskEntity.Status = taskRequest.Status;
            taskEntity.AssignedEmployee =
                _dbContext.Employees.First(employee => employee.Id == taskRequest.AssignedEmployeeId);
            taskEntity.Project = _dbContext.Projects.First(project => project.Id == taskRequest.ProjectId);
            taskEntity.Priority = taskRequest.Priority;
            taskEntity.Type = taskRequest.Type;

            await _dbContext.SaveChangesAsync();
            
            var checkUserManager = _dbContext.Managers.Any(manager => manager.Employee == user);

            if (checkUserManager)
            {
                var tasks = _dbContext.Tasks.Select(task => new TaskViewModel
                {
                    Name = task.Name,
                    Description = task.Description,
                    StartDate = task.StartDate,
                    EndDate = task.EndDate,
                    Status = task.Status,
                    Id = task.Id,
                    Priority = task.Priority,
                    Type = task.Type,
                    AssignerEmployeeName = task.AssignerEmployee.FirstName + " " + task.AssignerEmployee.LastName,
                    AssignedEmployeeName = task.AssignedEmployee.FirstName + " " + task.AssignedEmployee.LastName,
                    AssignedEmployeeId = task.AssignedEmployee.Id,
                    ProjectName = task.Project.Name
                }).ToList();
            
                return Ok(new Result<List<TaskViewModel>>
                (
                    success: true,
                    error: "",
                    data: tasks
                ));
            }
            else
            {
                var tasks = _dbContext.Tasks.Where(task => task.AssignedEmployee == user).Select(task => new TaskViewModel
                {
                    Name = task.Name,
                    Description = task.Description,
                    StartDate = task.StartDate,
                    EndDate = task.EndDate,
                    Status = task.Status,
                    Id = task.Id,
                    Priority = task.Priority,
                    Type = task.Type,
                    AssignerEmployeeName = task.AssignerEmployee.FirstName + " " + task.AssignerEmployee.LastName,
                    AssignedEmployeeName = task.AssignedEmployee.FirstName + " " + task.AssignedEmployee.LastName,
                    AssignedEmployeeId = task.AssignedEmployee.Id,
                    ProjectName = task.Project.Name
                }).ToList();
            
                return Ok(new Result<List<TaskViewModel>>
                (
                    success: true,
                    error: "",
                    data: tasks
                ));
            }
        }
        
        /// <summary>
        /// Employee
        /// Creates a task
        /// </summary>
        [HttpPost]
        [ProducesResponseType(typeof(Result<List<TaskViewModel>>), 200)]
        public async Task<IActionResult> CreateTask([FromBody] TaskRequest taskRequest)
        {
            var user = UserService.GetUser(AuthenticationHeaderValue.Parse(Request.Headers[HeaderNames.Authorization]), _dbContext);
            
            _dbContext.Tasks.Add(new Task
            {
                Id = Guid.NewGuid(),
                Name = taskRequest.Name,
                Description = taskRequest.Description,
                StartDate = taskRequest.StartDate,
                EndDate = taskRequest.EndDate,
                Status = 0,
                AssignerEmployee = user,
                AssignedEmployee = _dbContext.Employees.First(employee => employee.Id == taskRequest.AssignedEmployeeId),
                Project = _dbContext.Projects.First(project => project.Id == taskRequest.ProjectId),
                Priority = taskRequest.Priority,
                Type = taskRequest.Type
            });

            await _dbContext.SaveChangesAsync();
            
            var checkUserManager = _dbContext.Managers.Any(manager => manager.Employee == user);

            if (checkUserManager)
            {
                var tasks = _dbContext.Tasks.Select(task => new TaskViewModel
                {
                    Name = task.Name,
                    Description = task.Description,
                    StartDate = task.StartDate,
                    EndDate = task.EndDate,
                    Status = task.Status,
                    Id = task.Id,
                    Priority = task.Priority,
                    Type = task.Type,
                    AssignerEmployeeName = task.AssignerEmployee.FirstName + " " + task.AssignerEmployee.LastName,
                    AssignedEmployeeName = task.AssignedEmployee.FirstName + " " + task.AssignedEmployee.LastName,
                    AssignedEmployeeId = task.AssignedEmployee.Id,
                    ProjectName = task.Project.Name
                }).ToList();
            
                return Ok(new Result<List<TaskViewModel>>
                (
                    success: true,
                    error: "",
                    data: tasks
                ));
            }
            else
            {
                var tasks = _dbContext.Tasks.Where(task => task.AssignedEmployee == user).Select(task => new TaskViewModel
                {
                    Name = task.Name,
                    Description = task.Description,
                    StartDate = task.StartDate,
                    EndDate = task.EndDate,
                    Status = task.Status,
                    Id = task.Id,
                    Priority = task.Priority,
                    Type = task.Type,
                    AssignerEmployeeName = task.AssignerEmployee.FirstName + " " + task.AssignerEmployee.LastName,
                    AssignedEmployeeName = task.AssignedEmployee.FirstName + " " + task.AssignedEmployee.LastName,
                    AssignedEmployeeId = task.AssignedEmployee.Id,
                    ProjectName = task.Project.Name
                }).ToList();
            
                return Ok(new Result<List<TaskViewModel>>
                (
                    success: true,
                    error: "",
                    data: tasks
                ));
            }
        }
        
        
        /// <summary>
        /// Employee
        /// Get user tasks
        /// </summary>
        [HttpGet]
        [ProducesResponseType(typeof(Result<List<TaskViewModel>>), 200)]
        public async Task<IActionResult> Tasks()
        {
            var user = UserService.GetUser(AuthenticationHeaderValue.Parse(Request.Headers[HeaderNames.Authorization]), _dbContext);

            var checkUserManager = _dbContext.Managers.Any(manager => manager.Employee == user);

            if (checkUserManager)
            {
                var tasks = _dbContext.Tasks.Select(task => new TaskViewModel
                {
                    Name = task.Name,
                    Description = task.Description,
                    StartDate = task.StartDate,
                    EndDate = task.EndDate,
                    Status = task.Status,
                    Id = task.Id,
                    Priority = task.Priority,
                    Type = task.Type,
                    AssignerEmployeeName = task.AssignerEmployee.FirstName + " " + task.AssignerEmployee.LastName,
                    AssignedEmployeeName = task.AssignedEmployee.FirstName + " " + task.AssignedEmployee.LastName,
                    AssignedEmployeeId = task.AssignedEmployee.Id,
                    ProjectName = task.Project.Name
                }).ToList();
            
                return Ok(new Result<List<TaskViewModel>>
                (
                    success: true,
                    error: "",
                    data: tasks
                ));
            }
            else
            {
                var tasks = _dbContext.Tasks.Where(task => task.AssignedEmployee == user).Select(task => new TaskViewModel
                {
                    Name = task.Name,
                    Description = task.Description,
                    StartDate = task.StartDate,
                    EndDate = task.EndDate,
                    Status = task.Status,
                    Id = task.Id,
                    Priority = task.Priority,
                    Type = task.Type,
                    AssignerEmployeeName = task.AssignerEmployee.FirstName + " " + task.AssignerEmployee.LastName,
                    AssignedEmployeeName = task.AssignedEmployee.FirstName + " " + task.AssignedEmployee.LastName,
                    AssignedEmployeeId = task.AssignedEmployee.Id,
                    ProjectName = task.Project.Name
                }).ToList();
            
                return Ok(new Result<List<TaskViewModel>>
                (
                    success: true,
                    error: "",
                    data: tasks
                ));
            }
            
            
        }
    }
}