using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using PersonnelManagement.Models;
using PersonnelManagement.Models.RequestModels;
using PersonnelManagement.Models.ViewModels;
using PersonnelManagement.Services;

namespace PersonnelManagement.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class DepartmentController : Controller
    {
        
        private readonly PmDbContext _dbContext;

        public DepartmentController(PmDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        [HttpPost]
        [ProducesResponseType(typeof(Result<List<DepartmentViewModel>>), 200)]
        public async Task<IActionResult> Create([FromBody] DepartmentRequest departmentRequest)
        {
            
            var user = UserService.GetUser(AuthenticationHeaderValue.Parse(Request.Headers[HeaderNames.Authorization]), _dbContext);

            if (!_dbContext.Managers.Any(manager => manager.Employee == user))
            {
                
                HttpContext.Response.StatusCode = 401;
                return null;
            }
            
//        INSERT INTO Departments (Id, Name) VALUES ('id', 'Department_Name')
            
            _dbContext.Departments.Add(new Department
            {
                Id = Guid.NewGuid(),
                Name = departmentRequest.Name
            });

            await _dbContext.SaveChangesAsync();
            var departments = _dbContext.Departments.Select(department => new DepartmentViewModel
            {
                Id = department.Id,
                Name = department.Name
            }).ToList();
            
            return Ok(new Result<List<DepartmentViewModel>>
            (
                success: true,
                error: "",
                data: departments
            ));
        }
        
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(Result<List<DepartmentViewModel>>), 200)]
        public async Task<IActionResult> Update(Guid id, [FromBody] DepartmentRequest departmentRequest)
        {
            
            var user = UserService.GetUser(AuthenticationHeaderValue.Parse(Request.Headers[HeaderNames.Authorization]), _dbContext);

            if (!_dbContext.Managers.Any(manager => manager.Employee == user))
            {
                
                HttpContext.Response.StatusCode = 401;
                return null;
            }

            var departmentEntity = _dbContext.Departments.First(department => department.Id == id);

            departmentEntity.Name = departmentRequest.Name;

            await _dbContext.SaveChangesAsync();
            
//        UPDATE Departments SET Name = 'Update new' WHERE Id = 'update_id'
            
            var departments = _dbContext.Departments.Select(department => new DepartmentViewModel
            {
                Id = department.Id,
                Name = department.Name
            }).ToList();
            
            return Ok(new Result<List<DepartmentViewModel>>
            (
                success: true,
                error: "",
                data: departments
            ));
        }
        
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(Result<List<DepartmentViewModel>>), 200)]
        public async Task<IActionResult> Delete(Guid id)
        {
            
            var user = UserService.GetUser(AuthenticationHeaderValue.Parse(Request.Headers[HeaderNames.Authorization]), _dbContext);

            if (!_dbContext.Managers.Any(manager => manager.Employee == user))
            {
                
                HttpContext.Response.StatusCode = 401;
                return null;
            }

            _dbContext.Departments.Remove(_dbContext.Departments.First(department => department.Id == id));

            await _dbContext.SaveChangesAsync();
            
            
//        DELETE FROM `Departments` WHERE Id = 'department_id'
            
            var departments = _dbContext.Departments.Select(department => new DepartmentViewModel
            {
                Id = department.Id,
                Name = department.Name
            }).ToList();
            
            return Ok(new Result<List<DepartmentViewModel>>
            (
                success: true,
                error: "",
                data: departments
            ));
        }
        
            
        [HttpGet]
        [ProducesResponseType(typeof(Result<List<DepartmentViewModel>>), 200)]
        public async Task<IActionResult> Get()
        {
            
            var user = UserService.GetUser(AuthenticationHeaderValue.Parse(Request.Headers[HeaderNames.Authorization]), _dbContext);

            if (!_dbContext.Managers.Any(manager => manager.Employee == user))
            {
                
                HttpContext.Response.StatusCode = 401;
                return null;
            }
            
            
//        SELECT Id, Name FROM `Departments`
            
            var departments = _dbContext.Departments.Select(department => new DepartmentViewModel
            {
                Id = department.Id,
                Name = department.Name
            }).ToList();
            
            return Ok(new Result<List<DepartmentViewModel>>
            (
                success: true,
                error: "",
                data: departments
            ));
        }
    }
}