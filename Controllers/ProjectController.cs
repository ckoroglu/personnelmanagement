using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using PersonnelManagement.Models;
using PersonnelManagement.Models.RequestModels;
using PersonnelManagement.Models.ViewModels;
using PersonnelManagement.Services;

namespace PersonnelManagement.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class ProjectController : Controller
    {
        
        private readonly PmDbContext _dbContext;

        public ProjectController(IUserService userService, PmDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        /// <summary>
        /// Manager & Employee
        /// View all projects
        /// </summary>
        [HttpGet("GetAllProjects")]
        [ProducesResponseType(typeof(Result<List<ProjectViewModel>>), 200)]
        public async Task<IActionResult> GetAllProjects()
        {
            var projects = _dbContext.Projects.Select(project => new ProjectViewModel
            {
                Name = project.Name,
                Description = project.Description,
                StartDate = project.StartDate,
                EndDate = project.EndDate,
                Status = project.Status,
                Id = project.Id,
                CratedBy = project.Employee.FirstName + " " + project.Employee.LastName
            }).ToList();
            
            return Ok(new Result<List<ProjectViewModel>>
            (
                success: true,
                error: "",
                data: projects
            ));
        }
        
        /// <summary>
        /// Manager & Employee
        /// View all project tasks
        /// </summary>
        [HttpGet("{id}/Tasks")]
        [ProducesResponseType(typeof(Result<List<TaskViewModel>>), 200)]
        public async Task<IActionResult> Tasks(Guid id)
        {
            var tasks = _dbContext.Tasks.Where(task => task.ProjectId == id).Select(project => new TaskViewModel
            {
                Name = project.Name,
                Description = project.Description,
                StartDate = project.StartDate,
                EndDate = project.EndDate,
                Status = project.Status,
                Id = project.Id,
                Priority = project.Priority,
                Type = project.Type,
                AssignerEmployeeName = project.AssignerEmployee.FirstName + " " + project.AssignerEmployee.LastName,
                AssignedEmployeeName = project.AssignedEmployee.FirstName + " " + project.AssignedEmployee.LastName,
                AssignedEmployeeId = project.AssignedEmployee.Id
            }).ToList();
            
            return Ok(new Result<List<TaskViewModel>>
            (
                success: true,
                error: "",
                data: tasks
            ));
        }
        
        /// <summary>
        /// Employee
        /// Creates a project
        /// </summary>
        [HttpPost]
        [ProducesResponseType(typeof(Result<List<ProjectViewModel>>), 200)]
        public async Task<IActionResult> CreateProject([FromBody] ProjectRequest projectRequest)
        {
            var user = UserService.GetUser(AuthenticationHeaderValue.Parse(Request.Headers[HeaderNames.Authorization]), _dbContext);
            
            _dbContext.Projects.Add(new Project
            {
                Id = Guid.NewGuid(),
                Name = projectRequest.Name,
                Description = projectRequest.Description,
                StartDate = projectRequest.StartDate,
                EndDate = projectRequest.EndDate,
                Status = projectRequest.Status,
                Employee = user
            });

            await _dbContext.SaveChangesAsync();
            
            var projects = _dbContext.Projects.Select(project => new ProjectViewModel
            {
                Name = project.Name,
                Description = project.Description,
                StartDate = project.StartDate,
                EndDate = project.EndDate,
                Status = project.Status,
                Id = project.Id,
                CratedBy = project.Employee.FirstName + " " + project.Employee.LastName
            }).ToList();
            
            return Ok(new Result<List<ProjectViewModel>>
            (
                success: true,
                error: "",
                data: projects
            ));
        }
        
        /// <summary>
        /// Employee
        /// Updates a project
        /// </summary>
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(Result<List<ProjectViewModel>>), 200)]
        public async Task<IActionResult> UpdateProject(Guid id, [FromBody] ProjectRequest projectRequest)
        {

            var projectEntity = _dbContext.Projects.First(prj => prj.Id == id);

            projectEntity.Description = projectRequest.Description;
            projectEntity.Name = projectRequest.Name;
            projectEntity.StartDate = projectRequest.StartDate;
            projectEntity.EndDate = projectRequest.EndDate;
            projectEntity.Status = projectRequest.Status;

            await _dbContext.SaveChangesAsync();
            
            var projects = _dbContext.Projects.Select(project => new ProjectViewModel
            {
                Name = project.Name,
                Description = project.Description,
                StartDate = project.StartDate,
                EndDate = project.EndDate,
                Status = project.Status,
                Id = project.Id,
                CratedBy = project.Employee.FirstName + " " + project.Employee.LastName
            }).ToList();
            
            return Ok(new Result<List<ProjectViewModel>>
            (
                success: true,
                error: "",
                data: projects
            ));
        }
        
        /// <summary>
        /// Employee
        /// Deletes a project
        /// </summary>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(Result<List<ProjectViewModel>>), 200)]
        public async Task<IActionResult> DeleteProject(Guid id)
        {

            _dbContext.Projects.Remove(_dbContext.Projects.First(prj => prj.Id == id));
            await _dbContext.SaveChangesAsync();
            
            var projects = _dbContext.Projects.Select(project => new ProjectViewModel
            {
                Name = project.Name,
                Description = project.Description,
                StartDate = project.StartDate,
                EndDate = project.EndDate,
                Status = project.Status,
                Id = project.Id,
                CratedBy = project.Employee.FirstName + " " + project.Employee.LastName
            }).ToList();
            
            return Ok(new Result<List<ProjectViewModel>>
            (
                success: true,
                error: "",
                data: projects
            ));
        }
    }
}