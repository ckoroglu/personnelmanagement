using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Net.Http.Headers;
using PersonnelManagement.Models;
using PersonnelManagement.Models.RequestModels;
using PersonnelManagement.Models.ViewModels;
using PersonnelManagement.Services;

namespace PersonnelManagement.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly PmDbContext _dbContext;

        public UsersController(IUserService userService, PmDbContext dbContext)
        {
            _userService = userService;
            _dbContext = dbContext;
        }

        [AllowAnonymous]
        [HttpPost("Login")]
        [ProducesResponseType(typeof(Result<string>), 200)]
        public async Task<IActionResult> Authenticate([FromBody] LoginRequest userParam)
        {
            var user = await _userService.Authenticate(userParam.Email, userParam.Password);

            if (user == null)
                return Ok(new Result<bool>
                (
                    success: false,
                    error: "Username or password is incorrect",
                    data: false
                ));


            string txt = user.Email + ":" + user.Password;
            byte[] encodedBytes = System.Text.Encoding.UTF8.GetBytes(txt);
            string encodedTxt = Convert.ToBase64String(encodedBytes);

            return Ok(new Result<string>
            (
                success: true,
                error: "",
                data: encodedTxt
            ));
        }

        [HttpPost]
        [ProducesResponseType(typeof(Result<List<UserViewModel>>), 200)]
        public async Task<IActionResult> Create([FromBody] UserRequest userRequest)
        {
            var user = UserService.GetUser(AuthenticationHeaderValue.Parse(Request.Headers[HeaderNames.Authorization]),
                _dbContext);

            if (!_dbContext.Managers.Any(manager => manager.Employee == user))
            {
                HttpContext.Response.StatusCode = 401;
                return null;
            }

            var guid = Guid.NewGuid();
            _dbContext.Employees.Add(new Employee
            {
                Id = guid,
                FirstName = userRequest.FirstName,
                LastName = userRequest.LastName,
                Password = userRequest.Password,
                Email = userRequest.Email,
                StartDate = userRequest.StartDate,
                WorkingDayPerMonth = userRequest.WorkingDayPerMonth,
                Gender = userRequest.Gender,
                Address = userRequest.Address,
                PhoneNumber = userRequest.PhoneNumber,
                Department = _dbContext.Departments.First(department => department.Id == userRequest.DepartmentId),
                Salary = userRequest.Salary,
            });

            await _dbContext.SaveChangesAsync();

            if (userRequest.Role == "manager")
            {
                _dbContext.Managers.Add(new Manager
                {
                    Id = Guid.NewGuid(),
                    Employee = _dbContext.Employees.First(employee => employee.Id == guid)
                });
                await _dbContext.SaveChangesAsync();
            }

            var allUsers = _dbContext.Employees.Select(employee => new UserViewModel
            {
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                Email = employee.Email,
                PhoneNumber = employee.PhoneNumber,
                Address = employee.Address,
                Gender = employee.Gender,
                Status = employee.Leaves.Any(leave =>
                    leave.Status == 1 && DateTime.UtcNow >= leave.StartDate && DateTime.UtcNow <= leave.EndDate)
                    ? 0
                    : 1,
                WorkingDayPerMonth = employee.WorkingDayPerMonth,
                StartDate = employee.StartDate,
                Id = employee.Id,
                Salary = employee.Salary,
                Role = _dbContext.Managers.Any(manager => manager.Employee == employee)
                    ? "manager"
                    : "user",
                Department = employee.Department.Name
            }).ToList();

            return Ok(new Result<List<UserViewModel>>
            (
                success: true,
                error: "",
                data: allUsers
            ));
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(Result<List<UserViewModel>>), 200)]
        public async Task<IActionResult> Create(Guid id, [FromBody] UserRequest userRequest)
        {
            var user = UserService.GetUser(AuthenticationHeaderValue.Parse(Request.Headers[HeaderNames.Authorization]),
                _dbContext);

            if (!_dbContext.Managers.Any(manager => manager.Employee == user))
            {
                HttpContext.Response.StatusCode = 401;
                return null;
            }

            var getUser = _dbContext.Employees.FirstOrDefault(employee => employee.Id == id);
            if (getUser != null)
            {
                getUser.FirstName = userRequest.FirstName;
                getUser.LastName = userRequest.LastName;
                if (userRequest.Password != "******")
                {
                    getUser.Password = userRequest.Password;
                }

                getUser.Email = userRequest.Email;
                getUser.StartDate = userRequest.StartDate;
                getUser.WorkingDayPerMonth = userRequest.WorkingDayPerMonth;
                getUser.Gender = userRequest.Gender;
                getUser.Address = userRequest.Address;
                getUser.PhoneNumber = userRequest.PhoneNumber;
                getUser.Department =
                    _dbContext.Departments.First(department => department.Id == userRequest.DepartmentId);
                getUser.Salary = userRequest.Salary;

                if (_dbContext.Managers.Any(manager => manager.Employee == getUser) && userRequest.Role == "user")
                {
                    _dbContext.Remove(_dbContext.Managers.First(manager => manager.Employee == getUser));
                }
                else if (!_dbContext.Managers.Any(manager => manager.Employee == getUser) &&
                         userRequest.Role == "manager")
                {
                    _dbContext.Managers.Add(new Manager
                    {
                        Id = Guid.NewGuid(),
                        Employee = _dbContext.Employees.First(employee => employee.Id == id)
                    });
                }

                await _dbContext.SaveChangesAsync();
            }

            var allUsers = _dbContext.Employees.Select(employee => new UserViewModel
            {
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                Email = employee.Email,
                PhoneNumber = employee.PhoneNumber,
                Address = employee.Address,
                Gender = employee.Gender,
                Status = employee.Leaves.Any(leave =>
                    leave.Status == 1 && DateTime.UtcNow >= leave.StartDate && DateTime.UtcNow <= leave.EndDate)
                    ? 0
                    : 1,
                WorkingDayPerMonth = employee.WorkingDayPerMonth,
                StartDate = employee.StartDate,
                Id = employee.Id,
                Salary = employee.Salary,
                Role = _dbContext.Managers.Any(manager => manager.Employee == employee)
                    ? "manager"
                    : "user",
                Department = employee.Department.Name
            }).ToList();

            return Ok(new Result<List<UserViewModel>>
            (
                success: true,
                error: "",
                data: allUsers
            ));
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(Result<List<UserViewModel>>), 200)]
        public async Task<IActionResult> Delete(Guid id)
        {
            var user = UserService.GetUser(AuthenticationHeaderValue.Parse(Request.Headers[HeaderNames.Authorization]),
                _dbContext);

            if (!_dbContext.Managers.Any(manager => manager.Employee == user))
            {
                HttpContext.Response.StatusCode = 401;
                return null;
            }

            var employeeEntity = _dbContext.Employees.First(employee => employee.Id == id);
            if (_dbContext.Managers.Any(manager => manager.Employee == employeeEntity))
            {
                _dbContext.Managers.Remove(_dbContext.Managers.First(manager => manager.Employee == employeeEntity));
            }

            _dbContext.Employees.Remove(employeeEntity);
            await _dbContext.SaveChangesAsync();

            var allUsers = _dbContext.Employees.Select(employee => new UserViewModel
            {
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                Email = employee.Email,
                PhoneNumber = employee.PhoneNumber,
                Address = employee.Address,
                Gender = employee.Gender,
                Status = employee.Leaves.Any(leave =>
                    leave.Status == 1 && DateTime.UtcNow >= leave.StartDate && DateTime.UtcNow <= leave.EndDate)
                    ? 0
                    : 1,
                WorkingDayPerMonth = employee.WorkingDayPerMonth,
                StartDate = employee.StartDate,
                Id = employee.Id,
                Salary = employee.Salary,
                Role = _dbContext.Managers.Any(manager => manager.Employee == employee)
                    ? "manager"
                    : "user",
                Department = employee.Department.Name
            }).ToList();

            return Ok(new Result<List<UserViewModel>>
            (
                success: true,
                error: "",
                data: allUsers
            ));
        }

        [HttpGet]
        [ProducesResponseType(typeof(Result<UserViewModel>), 200)]
        public async Task<IActionResult> Get()
        {
            var user = UserService.GetUser(AuthenticationHeaderValue.Parse(Request.Headers[HeaderNames.Authorization]),
                _dbContext);

            var userModel = new UserViewModel
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber,
                Address = user.Address,
                Gender = user.Gender,
                Status = user.Leaves.Any(leave =>
                    leave.Status == 1 && DateTime.UtcNow >= leave.StartDate && DateTime.UtcNow <= leave.EndDate)
                    ? 0
                    : 1,
                WorkingDayPerMonth = user.WorkingDayPerMonth,
                StartDate = user.StartDate,
                Id = user.Id,
                Salary = user.Salary,
                Role = _dbContext.Managers.FirstOrDefault(manager => manager.Employee == user) != null
                    ? "manager"
                    : "user",
                Department = _dbContext.Departments.First(department => department.Id == user.DepartmentId).Name
            };

            return Ok(new Result<UserViewModel>
            (
                success: true,
                error: "",
                data: userModel
            ));
        }

//        SELECT `employee`.`FirstName`, `employee`.`LastName`, `employee`.`Email`, `employee`.`PhoneNumber`, `employee`.`Address`, `employee`.`Gender`, CASE
//            WHEN (
//                SELECT CASE
//        WHEN EXISTS (
//            SELECT 1
//        FROM `Leaves` AS `leave`
//        WHERE (((`leave`.`Status` = 1) AND (UTC_TIMESTAMP() >= `leave`.`StartDate`)) AND (UTC_TIMESTAMP() <= `leave`.`EndDate`)) AND (`employee`.`Id` = `leave`.`EmployeeId`))
//        THEN TRUE ELSE FALSE
//        END
//        ) = TRUE
//            THEN 0 ELSE 1
//        END AS `Status`, `employee`.`WorkingDayPerMonth`, `employee`.`StartDate`, `employee`.`Id`, `employee`.`Salary`, CASE
//            WHEN (
//                SELECT CASE
//        WHEN EXISTS (
//            SELECT 1
//        FROM `Managers` AS `manager`
//        WHERE `manager`.`EmployeeId` = `employee`.`Id`)
//        THEN TRUE ELSE FALSE
//        END
//        ) = TRUE
//            THEN 'manager' ELSE 'user'
//        END AS `Role`, `employee.Department`.`Name` AS `Department`
//        FROM `Employees` AS `employee`
//        INNER JOIN `Departments` AS `employee.Department` ON `employee`.`DepartmentId` = `employee.Department`.`Id`


        [HttpGet("GetAll")]
        [ProducesResponseType(typeof(Result<List<UserViewModel>>), 200)]
        public async Task<IActionResult> GetAll()
        {
//            var user = UserService.GetUser(AuthenticationHeaderValue.Parse(Request.Headers[HeaderNames.Authorization]), _dbContext);
//
//            if (!_dbContext.Managers.Any(manager => manager.Employee == user))
//            {
//                HttpContext.Response.StatusCode = 401;
//                return null;
//            }
//            
            var allUsers = _dbContext.Employees.Select(employee => new UserViewModel
            {
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                Email = employee.Email,
                PhoneNumber = employee.PhoneNumber,
                Address = employee.Address,
                Gender = employee.Gender,
                Status = employee.Leaves.Any(leave =>
                    leave.Status == 1 && DateTime.UtcNow >= leave.StartDate && DateTime.UtcNow <= leave.EndDate)
                    ? 0
                    : 1,
                WorkingDayPerMonth = employee.WorkingDayPerMonth,
                StartDate = employee.StartDate,
                Id = employee.Id,
                Salary = employee.Salary,
                Role = _dbContext.Managers.Any(manager => manager.Employee == employee)
                    ? "manager"
                    : "user",
                Department = employee.Department.Name
            }).ToList();

            return Ok(new Result<List<UserViewModel>>
            (
                success: true,
                error: "",
                data: allUsers
            ));
        }

        [HttpGet("DashboardInfo")]
        [ProducesResponseType(typeof(Result<ManagerDashboardViewModel>), 200)]
        public async Task<IActionResult> DashboardInfo()
        {
            var user = UserService.GetUser(AuthenticationHeaderValue.Parse(Request.Headers[HeaderNames.Authorization]),
                _dbContext);

            var userRole = _dbContext.Managers.FirstOrDefault(manager => manager.Employee == user);

            if (userRole != null)
            {
                var resultModel = new ManagerDashboardViewModel
                {
                    TotalEmployee = _dbContext.Employees.Count(),
                    TotalProject = _dbContext.Projects.Count(project => project.Status == 0),
                    TotalTask = _dbContext.Tasks.Count(task => task.Status == 0),
                    TotalLeaveRequest = _dbContext.Leaves.Count(leave => leave.Status == 0)
                };
                return Ok(new Result<ManagerDashboardViewModel>
                (
                    success: true,
                    error: "",
                    data: resultModel
                ));
            }
            else
            {
                var resultModel = new EmployeeDashboardViewModel
                {
                    TotalProject =
                        _dbContext.Projects.Count(project => project.Status == 0 && project.Employee == user),
                    TotalTask = _dbContext.Tasks.Count(task => task.Status == 0 && task.AssignedEmployee == user),
                    TotalLeaveRequest = _dbContext.Leaves.Count(leave => leave.Status == 0 && leave.Employee == user)
                };
                return Ok(new Result<EmployeeDashboardViewModel>
                (
                    success: true,
                    error: "",
                    data: resultModel
                ));
            }
        }
    }
}